# -*- coding: utf-8 -*-
from .log import LOGGER
from kit.rpc import rpc
from kit.tool.timer import Timer

from app_zhaobiao.bidding import Bidding


def keep_cookie():
    if rpc.state.cookies is None:
        return
    bidding = Bidding(rpc.state.cookies)
    try:
        res = bidding.check_login()
    except Exception as e:
        LOGGER.error(f'Failed to check login: {e}')
        return
    LOGGER.info(f"当前登录状态：{res}")


kc_timer = Timer("keep_cookie", keep_cookie)

