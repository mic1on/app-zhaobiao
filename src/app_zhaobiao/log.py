# -*- coding: utf-8 -*-
from loguru import logger


def register_log():
    return logger.bind(app_name="zhaobiao_cn")


LOGGER = register_log()
