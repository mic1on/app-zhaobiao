# -*- coding: utf-8 -*-
import httpx
from parsel import Selector
from kit.tool import get_useragent
headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Origin': 'https://s.zhaobiao.cn',
    'Pragma': 'no-cache',
    'Referer': 'https://s.zhaobiao.cn/s',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': get_useragent(),
    'sec-ch-ua': '"Chromium";v="106", "Microsoft Edge";v="106", "Not;A=Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"macOS"',
}


class Bidding:

    def __init__(self, cookies):
        self.cookies = cookies

    def crawl_list(self, keyword, page=1, field=None, search_type=None, channels=None):
        """
        搜索列表
        :param keyword: 关键词
        :param page: 页码
        :param field: 标题搜索:title/全文搜索:all/超级搜索:super
        :param search_type: 商机:sj/招标:zb/项目:xm
        :param channels: channel list ['bidding', 'fore', 'purchase', 'free']
        :return:
        """
        field = field or 'title'
        search_type = search_type or 'zb'
        if channels is None:
            channels = []
        url = "https://s.zhaobiao.cn/s"

        data = {
            'queryword': keyword,
            'searchModel': '1',
            'attachment': '1',
            'field': field,
            'provinces': '',
            'channels': ','.join(channels),
            'searchtype': search_type,
            'currentpage': page,
        }
        response = httpx.post(
            url=url,
            headers=headers,
            cookies=self.cookies,
            data=data
        )
        parser = Selector(response.text)
        items = parser.xpath("//tr[@class='datatr']")
        result = []
        for item in items:
            _title = item.xpath("./td[2]/a/span//text()").getall()
            _title = ''.join(_title)
            _item = {
                "type": item.xpath("./td[1]/text()").get(),
                "title": _title,
                "url": item.xpath("./td[2]/a/@href").get(),
                "locate": item.xpath("./td[3]/text()").get(),
                "publish_date": item.xpath("./td[4]/text()").get(),
                "field": field
            }
            result.append(_item)
        return result

    def check_login(self):
        """检查登录状态"""
        url = "https://bs.zhaobiao.cn/login/getUserInfoAjax"
        response = httpx.post(url, headers=headers, cookies=self.cookies)
        if response.json().get('status', '') == '200':
            return True
        return False


if __name__ == '__main__':
    bidding = Bidding(
        {"Cookies_token": "e91f72f7-cc45-434b-ae4f-382898ca5729"}
    )
    print(bidding.check_login())
