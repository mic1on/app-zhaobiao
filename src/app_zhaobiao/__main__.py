# -*- coding: utf-8 -*-
from kit.rpc import rpc
from kit.rpc.store import set_store
from kit.store.rabbitmq import RabbitmqStore
from app_zhaobiao.config import RMQ
from app_zhaobiao.log import LOGGER
# from app_zhaobiao.keep_cookie import kc_timer

store = RabbitmqStore(
    host=RMQ.host,
    port=RMQ.port,
    username=RMQ.username,
    password=RMQ.password,
    ssl=RMQ.ssl
)
set_store(store)


def main():
    from app_zhaobiao.job.bidding_list import job_zhaobiao_list
    LOGGER.warning('启动成功')

    # if not kc_timer.alive():
    #     kc_timer.scheduler()
    rpc.run_forever()


if __name__ == '__main__':
    main()
