# -*- coding: utf-8 -*-
from kit.rpc import rpc
from kit.rpc.broker import Broker

from ..bidding import Bidding
from ..log import LOGGER

from_zhaobiao_list = Broker("job.zhaobiao.list")
to_zhaobiao_result = Broker("job.zhaobiao.result")


@rpc.job(from_broker=from_zhaobiao_list, to_broker=to_zhaobiao_result)
def job_zhaobiao_list(message, *args, **kwargs):
    rpc.state.cookies = message.kwargs.cookies
    data = message.kwargs.data or []

    bidding = Bidding(rpc.state.cookies)

    if rpc.state.cookies and not bidding.check_login():
        LOGGER.error("need login")
        return None

    for item in data:
        page = item.page or 1
        for p in range(1, page + 1):
            LOGGER.info(f'[{page}/{item.keyword}] start crawl zhaobiao list', extra={'keyword': item.keyword, 'page': p})
            try:
                bidding_list = bidding.crawl_list(
                    keyword=item.keyword,
                    page=p,
                    field=item.field,
                    search_type=item.search_type,
                    channels=item.channels
                )
                # bidding_list = [{**message, 'item': item} for item in bidding_list]
            except Exception as e:
                LOGGER.error(f'Failed to crawl zhaobiao list: {e}')
                continue
            yield from bidding_list
